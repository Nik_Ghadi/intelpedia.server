﻿using IntelPedia.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public class UnitOWork : IUnitOfWork
    {
        private IPDataModel db;
        private IUser _user;
        private IBrandTestRepository _brandtest;

        public void UnitOfWork(IPDataModel context)
        {
            db = context;
        }

        public IUser user
        {
            get
            {
                return _user = _user ?? new UserRepository(db);
            }
        }

        public IBrandTestRepository brandtest
        {
            get
            {
                return _brandtest = _brandtest ?? new BrandTestRepository(db);
            }
        }
        public void save()
        {
            db.SaveChanges();
        }
    }

}
