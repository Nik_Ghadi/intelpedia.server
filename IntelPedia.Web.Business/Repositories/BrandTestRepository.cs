﻿using IntelPedia.Web.Data;
using IntelPedia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public class BrandTestRepository : IBrandTestRepository
    {
        private IPDataModel _context;

        public BrandTestRepository(IPDataModel context)
        {
            _context = context;
        }
        public List<vQuestionFeedackReport> getBrandTestQuestions()
        {
            List<vQuestionFeedackReport> QList = new List<vQuestionFeedackReport>();
            List<question> QuestionList = new List<question>();
            QuestionList = _context.questions
                .ToList();
            if(QuestionList.Count > 0)
            {
                foreach (question q in QuestionList)
                {
                    vQuestionFeedackReport qfr = new vQuestionFeedackReport();
                    qfr.q_id = q.q_id;
                    qfr.qtextEnglish = q.qtextEnglish;
                    qfr.qtextMarathi = q.qtextMarathi;
                    QList.Add(qfr);
                }
            }
            return QList;
        }

        public string submitTest(BrandTestModels btModel)
        {
            try
            {
                if (btModel.qlist.Count > 0)
                {
                    foreach (vQuestionFeedackReport vQFR in btModel.qlist)
                    {
                        questionUserFeedback quf = new questionUserFeedback();
                        quf.createdon = DateTime.Now;
                        quf.q_id = vQFR.q_id;
                        quf.u_id = btModel.user.u_id;
                        quf.yesorno = vQFR.yesorno;
                        _context.questionUserFeedbacks.Add(quf);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Success";
        }
    }
}
