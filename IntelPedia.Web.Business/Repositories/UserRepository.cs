﻿using IntelPedia.Web.Data;
using IntelPedia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public class UserRepository : IUser
    {
        private IPDataModel _context;

        public UserRepository(IPDataModel context)
        {
            _context = context;
        }

        public IEnumerable<user> getAll()
        {
            return _context.users.ToList();
        }

        public user getUserByUserId(int uid)
        {
            user user = _context.users.Where(x => x.u_id == uid).FirstOrDefault();
            return user;
        }

        public user Insert(user user)
        {
            _context.users.Add(user);
            _context.SaveChanges();
            return user;
        }

        public void Update(user user)
        {
            //_context.users.(user);
        }
        public void Delete(user user)
        {
            _context.users.Remove(user);
        }
    }
}
