﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public interface IUnitOfWork
    {
        IUser user { get; }
        IBrandTestRepository brandtest { get; }
        void save();
    }
}
