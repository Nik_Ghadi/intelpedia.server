﻿using IntelPedia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public interface IBrandTestRepository
    {
        List<vQuestionFeedackReport> getBrandTestQuestions();

        string submitTest(BrandTestModels btModel);
    }
}
