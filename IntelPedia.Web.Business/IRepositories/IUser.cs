﻿using IntelPedia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Business
{
    public interface IUser
    {
        IEnumerable<user> getAll();

        user getUserByUserId(int userid);

        user Insert(user user);
        void Update(user user);
        void Delete(user user);
    }
}
