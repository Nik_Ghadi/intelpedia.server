﻿using IntelPedia.Web.Business;
using IntelPedia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IntelPedia.Web.API.Controllers
{
    [RoutePrefix("api/brandtest")]
    public class BrandTestController : ApiController
    {
        private IUser _user;
        private IBrandTestRepository _brandtest;
      

        public BrandTestController(IUser user,IBrandTestRepository brandtest)
        {
            _user = user;
            _brandtest = brandtest;
        }
        [Route("getquestions")]
        public IHttpActionResult GetBrandTestQuestions()
        {
            List<vQuestionFeedackReport> btQuestionList = new List<vQuestionFeedackReport>();

            btQuestionList = _brandtest.getBrandTestQuestions();
            if(btQuestionList == null)
            {
                btQuestionList = new List<vQuestionFeedackReport>();
            }
            return Ok(btQuestionList);
        }

        [HttpPost]
        [Route("submit")]
        public IHttpActionResult SubmitTest(BrandTestModels btModel)
        {

            if(btModel == null)
            {
                return Ok(new { status = "Error",message="empty data!!!" });
            }
            if (btModel.user == null)
            {
                return Ok(new { status = "Error", message = "empty user data!!!" });
            }
            if (btModel.qlist == null || btModel.qlist.Count == 0)
            {
                return Ok(new { status = "Error", message = "empty Question data!!!" });
            }
            if (String.IsNullOrEmpty(btModel.user.firstname) || String.IsNullOrEmpty(btModel.user.lastname))
            {
                return Ok(new { status = "Error", message = "please Enter Name!!!" });
            }
            user u = _user.Insert(btModel.user);

            btModel.user = u;

            string resp = _brandtest.submitTest(btModel);

            return Ok(new { status = resp });
        }
    }
}
