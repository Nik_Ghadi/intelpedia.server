namespace IntelPedia.Web.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using IntelPedia.Web.Models;

    public partial class IPDataModel : DbContext
    {
        public IPDataModel()
            : base("name=IPDataModel")
        {
        }

        public virtual DbSet<user> users { get; set; }

        public virtual DbSet<question> questions { get; set; }
        public virtual DbSet<questionUserFeedback> questionUserFeedbacks { get; set; }
        public virtual DbSet<vQuestionFeedackReport> vQuestionFeedackReports { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
