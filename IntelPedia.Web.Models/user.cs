namespace IntelPedia.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class user
    {
        [Key]
        public int u_id { get; set; }

        [Required]
        [StringLength(255)]
        public string username { get; set; }

        [Required]
        [StringLength(255)]
        public string firstname { get; set; }

        [StringLength(255)]
        public string middlename { get; set; }

        [StringLength(255)]
        public string lastname { get; set; }

        public int? genderid { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string mobileno
        {
            get; set;
        }
        [StringLength(255)]
        public string city
        {
            get; set;
        }
          
        public DateTime? dateofbirth { get; set; }

        public bool active { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime createdon { get; set; }

        [Required]
        [StringLength(255)]
        public string aspnetuserid { get; set; }

        [StringLength(255)]
        public string referby { get; set; }

        [StringLength(255)]
        public string referbythrough { get; set; }

        [StringLength(255)]
        public string prefLanguage { get; set; }
        

        [Column(TypeName = "smalldatetime")]
        public DateTime? lastlogin { get; set; }

        public int? secretquestionno { get; set; }

        [StringLength(255)]
        public string secretanswer { get; set; }
    }
}
