namespace IntelPedia.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    public partial class question
    {
        [Key]
        public int q_id { get; set; }

        [Required]
        public string qtextEnglish { get; set; }

        [Required]
        public string qtextMarathi { get; set; }

        public bool active { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime createdon { get; set; }
    }
}
