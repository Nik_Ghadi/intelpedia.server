namespace IntelPedia.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("vQuestionFeedackReport")]
    public partial class vQuestionFeedackReport
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int quf_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int q_id { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int u_id { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool yesorno { get; set; }

        [Key]
        [Column(Order = 4, TypeName = "smalldatetime")]
        public DateTime createdon { get; set; }

        [Key]
        [Column(Order = 5)]
        public string qtextEnglish { get; set; }

        [Key]
        [Column(Order = 6)]
        public string qtextMarathi { get; set; }

        [Key]
        [Column(Order = 7)]
        public bool active { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(255)]
        public string username { get; set; }

        [Key]
        [Column(Order = 9)]
        [StringLength(255)]
        public string firstname { get; set; }

        [StringLength(255)]
        public string middlename { get; set; }

        [StringLength(255)]
        public string lastname { get; set; }

        public int? genderid { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        public DateTime? dateofbirth { get; set; }

        [Key]
        [Column(Order = 10)]
        public bool useractive { get; set; }

        [Key]
        [Column(Order = 11)]
        [StringLength(255)]
        public string aspnetuserid { get; set; }

        [StringLength(255)]
        public string referby { get; set; }

        [StringLength(255)]
        public string referbythrough { get; set; }

        [StringLength(255)]
        public string prefLanguage { get; set; }

    }
}
