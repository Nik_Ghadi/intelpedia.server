namespace IntelPedia.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
   

    [Table("questionUserFeedback")]
    public partial class questionUserFeedback
    {
        [Key]
        public int quf_id { get; set; }

        public int q_id { get; set; }

        public int u_id { get; set; }

        public bool yesorno { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime createdon { get; set; }
    }
}
