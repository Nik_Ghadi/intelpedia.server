﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelPedia.Web.Models
{
    public class BrandTestModels
    {
        public user user { get; set; }

        public List<vQuestionFeedackReport> qlist { get; set; }

        public BrandTestModels()
        {
            user = new user();
            qlist = new List<vQuestionFeedackReport>();
        }
        public BrandTestModels(user u)
        {
            user = u;
            qlist = new List<vQuestionFeedackReport>();
        }
        public BrandTestModels(List<vQuestionFeedackReport> q)
        {
            user = new user();
            qlist = q;
        }
        public BrandTestModels(user u, List<vQuestionFeedackReport> q)
        {
            user = u;
            qlist = q;
        }
    }
}
